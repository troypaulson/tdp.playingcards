// Lab Excercise 2 - Playing Cards
// Troy Paulson

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit {
	Clubs,
	Spades,
	Hearts,
	Diamonds
};

struct Card {
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case Two: cout << "the Two of"; break;
	case Three: cout << "the Three of"; break;
	case Four: cout << "the Four of"; break;
	case Five: cout << "the Five of"; break;
	case Six: cout << "the Six of"; break;
	case Seven: cout << "the Seven of"; break;
	case Eight: cout << "the Eight of"; break;
	case Nine: cout <<  "the Nine of"; break;
	case Ten: cout << "the Ten of"; break;
	case Jack: cout << "the Jack of"; break;
	case Queen: cout << "the Queen of"; break;
	case King: cout << "the King of"; break;
	case Ace: cout << "the Ace of"; break;
	}

	switch (card.Suit)
	{
	case Spades: cout << "Spades\n"; break;
	case Hearts: cout << "Hearts\n"; break;
	case Clubs: cout << "Clubs\n"; break;
	case Diamonds: cout << "Diamonds\n"; break;

	}
}

Card GetHighCard(Card c1, Card c2)
{
	if (c1.Rank > c2.Rank)
	return c1;
	return c2;
}


int main() 

{	
	Card c1;
	c1.Rank = Two;
	c1.Suit - Hearts;
	PrintCard(c1);
	
	
	Card c2;
	c2.Rank = Four;
	c2.Suit = Spades;

	PrintCard(GetHighCard(c1, c2));
	


	_getch();
	return 0;
}